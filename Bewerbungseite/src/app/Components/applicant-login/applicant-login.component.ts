import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApplicantService} from "../../Services/applicant/applicant.service";
import {Applicant} from "../../Models/applicant";
import {Router} from "@angular/router";

@Component({
  selector: 'app-applicant-login',
  templateUrl: './applicant-login.component.html',
  styleUrls: ['./applicant-login.component.css']
})
export class ApplicantLoginComponent implements OnInit {
  loginForm!: FormGroup;
  wrongCredentials = false;

  constructor(private lf: FormBuilder, private userService: ApplicantService, private router: Router) {
  }

  ngOnInit(): void {
    this.loginForm = this.lf.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['', [Validators.required]],
    });
  }


  submethode() {
    let values: any = this.loginForm.value;

    if (this.loginForm.valid) {
      this.userService.postApplicantLogin(values.email, values.passwort).subscribe((data: Applicant) => {
          console.log(data);
          localStorage.setItem("alleApplicantData", JSON.stringify(data));
          this.router.navigate(['/applicant-profile']).then(() =>
            setTimeout(() => {
              window.location.reload();
            }, 1000)
          );
        },
        error => {
          console.log(!error.errorMessage);
        });
    }
  }

}
