import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApplicantService} from "../../Services/applicant/applicant.service";
import {AuthService} from "../../Services/auth.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-application-formular',
  templateUrl: './application-formular.component.html',
  styleUrls: ['./application-formular.component.css']
})
export class ApplicationFormularComponent implements OnInit {
  applicationsForm!: FormGroup;
  applicantObject!: any;
  isLoggedIn!: any;
  file = new FormData();
  pdfFiles: File[] = [];

  constructor(private fb: FormBuilder, private userService: ApplicantService,
              private authservice: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.applicationsForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(300)]],
      firstName: ['', [Validators.required, Validators.maxLength(300)]],
      dayOfBirth: ['', [Validators.required, Validators.maxLength(300)]],
      gender: ['', [Validators.required,]],
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      phoneNumber: ['', [Validators.required,]],
      streetNumber: ['', [Validators.required, Validators.maxLength(300)]],
      postcode: ['', [Validators.required, Validators.maxLength(300)]],
      city: ['', [Validators.required, Validators.maxLength(300)]],
      country: ['', [Validators.required, Validators.maxLength(300)]],
      file: ['', [Validators.required, Validators.maxLength(300)]],
    })

    this.displayApplicationForm();
    this.checkLoggedIn();
  }

  displayApplicationForm() {
    this.applicantObject = JSON.parse(String(localStorage.getItem("alleApplicantData")));
    this.applicationsForm.controls['name'].setValue(this.applicantObject?.name);
    this.applicationsForm.controls['firstName'].setValue(this.applicantObject?.firstName);
    this.applicationsForm.controls['email'].setValue(this.applicantObject?.email);
    this.applicationsForm.controls['dayOfBirth'].setValue(this.applicantObject?.dayOfBirth);
    this.applicationsForm.controls['gender'].setValue(this.applicantObject?.gender);
    this.applicationsForm.controls['phoneNumber'].setValue(this.applicantObject?.phoneNumber);
    this.applicationsForm.controls['streetNumber'].setValue(this.applicantObject?.address.streetNumber);
    this.applicationsForm.controls['postcode'].setValue(this.applicantObject?.address.postcode);
    this.applicationsForm.controls['city'].setValue(this.applicantObject?.address.city);
    this.applicationsForm.controls['country'].setValue(this.applicantObject?.address.country);

  }

  checkLoggedIn(): Boolean {
    let response = this.authservice.isLoggedIn();
    this.isLoggedIn = response;
    return this.isLoggedIn;
  }

  applicationWithAccount() {
    const pdfFileFormData = new FormData();
    for (var i = 0; i<this.pdfFiles.length; i++){
      pdfFileFormData.append("files", this.pdfFiles[i])
    }
    this.file = this.applicationsForm.value.file;
    this.applicationsForm.value.file = pdfFileFormData;
    let positionId = localStorage.getItem("positionId");
    let applicantId = localStorage.getItem("loggedId");
    if (this.applicationsForm.valid) {
      this.userService.createApplication(this.applicationsForm.value ,Number(applicantId), Number(positionId))
        .subscribe( (data) =>{
          console.log(data);
          this.router.navigate(['/confirmation-message']);
        }, error => {
          console.log(!error.message());
        });

    }
  }

  applicationNoAccount() {
    const pdfFileFormData = new FormData();
    for (var i = 0; i<this.pdfFiles.length; i++){
      pdfFileFormData.append("files", this.pdfFiles[i])
    }
    this.file = this.applicationsForm.value.file;
    this.applicationsForm.value.file = pdfFileFormData;
    let positionId = localStorage.getItem("positionId");
    if (this.applicationsForm.valid) {
      this.userService.createNoAccountApplication(Number(positionId), this.applicationsForm.value)
        .subscribe( (data) =>{
          console.log(data);
          this.router.navigate(['/confirmation-message']);
        }, error => {
          console.log(!error.message());
          });

    }
  }

  // Überprüft ob eine File ausgewählt wurde
  onFileChange(taget: any): void {
    this.pdfFiles = taget.files;
  }
}
