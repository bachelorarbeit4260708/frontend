import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../Services/auth.service";


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

   verify: boolean = localStorage.getItem("isSchool") == "true";

  constructor(private router: Router, private auth: AuthService) { }

  ngOnInit(): void {
this.isLoggedIn();
  }

  logout(){
    this.auth.logout();
  }
  isLoggedIn(): boolean{
     return this.auth.isLoggedIn();
  }
}
