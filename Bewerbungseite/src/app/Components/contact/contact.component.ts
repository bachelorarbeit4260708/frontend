import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApplicantService} from "../../Services/applicant/applicant.service";
import {Contact} from "../../Models/contact";
import {Router} from "@angular/router";


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contactform!: FormGroup;

  constructor(private fb: FormBuilder, private userService: ApplicantService, private router: Router) {
  }

  ngOnInit(): void {
    this.contactform = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      message: ['', [Validators.required]]
    });
  }

  sendContact() {
    let formValue: any = this.contactform.value;
    if (this.contactform.valid) {
      let result: Contact = Object.assign({}, formValue);
      this.userService.postContactData(result).subscribe((data: Contact) => {
          this.router.navigate(['']);
        },
        error => {
          console.log(!error.errorMessage);
        });
    }
  }

}
