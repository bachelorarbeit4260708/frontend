import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SchoolServiceService} from "../../Services/school/school-service.service";
import {Position} from "../../Models/position";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-add-training-place',
  templateUrl: './add-training-place.component.html',
  styleUrls: ['./add-training-place.component.css']
})
export class AddTrainingPlaceComponent implements OnInit {

  trainingform!: FormGroup;
  positionId!: number;
  isEditedMod: Boolean = false;
  position: Position = {
    id: 0,
    jobTitel: "",
    jobDescription: " ",
    offer: "",
    yourProfile: " ",
    contactPersonName: "",
    contactPersonEmail: "",
  };
  connectedSchoolId = localStorage.getItem("loggedId");

  constructor(private fb: FormBuilder, private schoolService: SchoolServiceService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.positionId = Number(this.route.snapshot.paramMap.get('id'));
    if (this.positionId !== undefined && this.positionId !== null && this.positionId !== 0) {
      this.getPositionById(this.positionId)
      this.isEditedMod = true;
    } else {
      this.createForm();
    }
  }

  // Erzeugt einen Ausbildungsplatz
  addPosition() {
    let values: any = this.trainingform.value;
    console.log(this.connectedSchoolId)
    if (this.trainingform.valid) {
      this.schoolService.postAddPosition(values, Number(this.connectedSchoolId))
        .subscribe((data: Position) => {
            this.router.navigate(['/schools-position']);
          },
          error => {
            console.log(!error.errorMessage);
          });
    }
  }

// Ruft einen Ausbildungsplatz mit Hilfe der schoolId
  getPositionById(id: number): void {
    this.schoolService.getPositionById(id).subscribe((data: Position) => {
        this.position = data;
        this.createForm()
      },
      error => {
        console.log(!error.errorMessage);
      });
    ;
  }

  public createForm(): void {
    this.trainingform = this.fb.group({
      id: [this.position?.id],
      jobTitel: [this.position?.jobTitel, [Validators.required, Validators.maxLength(350)]],
      jobDescription: [this.position?.jobDescription, [Validators.required, Validators.maxLength(100000)]],
      offer: [this.position?.offer, [Validators.required, Validators.maxLength(100000)]],
      yourProfile: [this.position?.yourProfile, [Validators.required, Validators.maxLength(100000)]],
      contactPersonName: [this.position?.contactPersonName, [Validators.required, Validators.maxLength(350)]],
      contactPersonEmail: [this.position?.contactPersonEmail, [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]]
    })
  }

  updatePosition() {
    let values: any = this.trainingform.value;
    console.log(this.connectedSchoolId)
    if (this.trainingform.valid) {
      this.schoolService.postUpdatePosition(values)
        .subscribe((data: Position) => {
            this.router.navigate(['/schools-position'])
          },
          error => {
            console.log(!error.errorMessage);
          });
    }
  }

  // Sendet das Formular
  submit(): void {
    if (this.isEditedMod) {
      this.updatePosition()
    } else {
      this.addPosition();
    }
  }
}
