import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTrainingPlaceComponent } from './add-training-place.component';

describe('AddTrainingPlaceComponent', () => {
  let component: AddTrainingPlaceComponent;
  let fixture: ComponentFixture<AddTrainingPlaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTrainingPlaceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddTrainingPlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
