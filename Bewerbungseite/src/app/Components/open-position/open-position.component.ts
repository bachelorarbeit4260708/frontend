import { Component, OnInit } from '@angular/core';
import {SchoolServiceService} from "../../Services/school/school-service.service";
import {Position, SchoolPosition} from "../../Models/position";

@Component({
  selector: 'app-open-position',
  templateUrl: './open-position.component.html',
  styleUrls: ['./open-position.component.css']
})
export class OpenPositionComponent implements OnInit {

  constructor(private schoolService: SchoolServiceService) {
  }

  ngOnInit(): void {
    this.getPositionList();
  }


  positionList!: SchoolPosition[];


  public getPositionList(): void {
   this.schoolService.getPositionList().subscribe(
      data => {
        //localStorage.setItem("AllList", JSON.stringify(data));
        console.log(data);
        this.positionList = data;
      },
      error => {
        console.log(error);

      });
  }
}
