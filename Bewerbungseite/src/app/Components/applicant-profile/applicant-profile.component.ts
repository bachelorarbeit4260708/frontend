import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApplicantService} from "../../Services/applicant/applicant.service";

import {Applicant} from "../../Models/applicant";
import {Router} from "@angular/router";
import {AuthService} from "../../Services/auth.service";

@Component({
  selector: 'app-applicant-profile',
  templateUrl: './applicant-profile.component.html',
  styleUrls: ['./applicant-profile.component.css']
})
export class ApplicantProfileComponent implements OnInit {
  applicantform!: FormGroup;

  applicantObject!: any;

  constructor(private fb: FormBuilder, private userService: ApplicantService, private router: Router, private auth: AuthService) {
  }

  ngOnInit(): void {
    this.applicantform = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(300)]],
      firstName: ['', [Validators.required, Validators.maxLength(300)]],
      dayOfBirth: ['', [Validators.required]],
      gender: [''],
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      phoneNumber: [''],
      streetNumber: ['', [Validators.required, Validators.maxLength(300)]],
      postcode: ['', [Validators.required, Validators.maxLength(300)]],
      city: ['', [Validators.required, Validators.maxLength(300)]],
      country: ['', [Validators.required, Validators.maxLength(300)]],

    });
    this.displayApplicantProfileData();

  }


// Aktualisiert Daten auf das Formular
  displayApplicantProfileData() {
    this.applicantObject = JSON.parse(String(localStorage.getItem("alleApplicantData")));
    this.applicantform.controls['name'].setValue(this.applicantObject?.name);
    this.applicantform.controls['firstName'].setValue(this.applicantObject?.firstName);
    this.applicantform.controls['email'].setValue(this.applicantObject?.email);
    this.applicantform.controls['dayOfBirth'].setValue(this.applicantObject?.dayOfBirth);
    this.applicantform.controls['gender'].setValue(this.applicantObject?.gender);
    this.applicantform.controls['phoneNumber'].setValue(this.applicantObject?.phoneNumber);
    this.applicantform.controls['streetNumber'].setValue(this.applicantObject?.address.streetNumber);
    this.applicantform.controls['postcode'].setValue(this.applicantObject?.address.postcode);
    this.applicantform.controls['city'].setValue(this.applicantObject?.address.city);
    this.applicantform.controls['country'].setValue(this.applicantObject?.address.country);

  }

  updateFromData() {
    let values: any = this.applicantform.value;
    let id = localStorage.getItem("loggedId");
    if (this.applicantform.valid) {
      this.userService.putApplicantData(Number(id), values)
        .subscribe((data: Applicant) => {
            this.auth.logout();
          },
          error => {
            console.log(error.error.errorMessage);
          });
    }
  }

}
