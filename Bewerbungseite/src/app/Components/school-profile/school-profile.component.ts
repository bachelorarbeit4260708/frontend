import {Component, OnInit} from '@angular/core';
import {SchoolServiceService} from "../../Services/school/school-service.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {School} from "../../Models/school";
import {Router} from "@angular/router";
import {AuthService} from "../../Services/auth.service";

@Component({
  selector: 'app-school-profile',
  templateUrl: './school-profile.component.html',
  styleUrls: ['./school-profile.component.css']
})
export class SchoolProfileComponent implements OnInit {
  schoolForm!: FormGroup

  loggedschool = localStorage.getItem("loggedId")

  schoolObject!: any;

  constructor(private fb: FormBuilder, private schoolService: SchoolServiceService, private router: Router, private auth: AuthService) {
  }

  ngOnInit(): void {
    this.schoolForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(300)]],
      streetNumber: ['', [Validators.required, Validators.maxLength(300)]],
      postcode: ['', [Validators.required, Validators.maxLength(300)]],
      city: ['', [Validators.required, Validators.maxLength(300)]],
      country: ['', [Validators.required, Validators.maxLength(300)]],
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      phoneNumber: ['', [Validators.required]],
      formationType: ['', Validators.required]
    })

    this.displaySchoolProfileData();
  }


  displaySchoolProfileData() {
    this.schoolObject = JSON.parse(String(localStorage.getItem("allSchoolData")));
    this.schoolForm.controls['name'].setValue(this.schoolObject?.name);
    this.schoolForm.controls['streetNumber'].setValue(this.schoolObject?.address.streetNumber);
    this.schoolForm.controls['postcode'].setValue(this.schoolObject?.address.postcode);
    this.schoolForm.controls['city'].setValue(this.schoolObject?.address.city);
    this.schoolForm.controls['country'].setValue(this.schoolObject?.address.country);
    this.schoolForm.controls['email'].setValue(this.schoolObject?.email);
    this.schoolForm.controls['formationType'].setValue(this.schoolObject?.formationType);
    this.schoolForm.controls['phoneNumber'].setValue(this.schoolObject?.phoneNumber);
  }

  submethode() {
    let values: any = this.schoolForm.value;
    if (this.schoolForm.valid) {
      this.schoolService.putSchoolProfile(Number(this.loggedschool), values).subscribe((data: School) => {
          this.auth.logout();
        },
        error => {
          console.log(error.error.errorMessage);
        });
    }
  }
}
