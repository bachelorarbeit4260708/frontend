import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantFavoriteListComponent } from './applicant-favorite-list.component';

describe('UserFavoriteListComponent', () => {
  let component: ApplicantFavoriteListComponent;
  let fixture: ComponentFixture<ApplicantFavoriteListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplicantFavoriteListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ApplicantFavoriteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
