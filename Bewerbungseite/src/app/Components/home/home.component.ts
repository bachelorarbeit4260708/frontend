import {Component, OnInit} from '@angular/core';
import {SchoolPosition} from "../../Models/position";
import {SchoolServiceService} from "../../Services/school/school-service.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private schoolService: SchoolServiceService) {
  }

  ngOnInit(): void {
    this.getTwoPosition();
  }

  twoPositionElement!: SchoolPosition[];
  islisteEmpty: any;

  public getTwoPosition(): void {
    this.schoolService.getPosition().subscribe(data => {
        this.twoPositionElement = data;
        this.islisteEmpty = this.twoPositionElement.length <= 1 ? true : false;
      },
      error => {
        console.log(error);
      }
    )
  }
}
