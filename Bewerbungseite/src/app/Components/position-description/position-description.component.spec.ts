import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionDescriptionComponent } from './position-description.component';

describe('PositionDescriptionComponent', () => {
  let component: PositionDescriptionComponent;
  let fixture: ComponentFixture<PositionDescriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionDescriptionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PositionDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
