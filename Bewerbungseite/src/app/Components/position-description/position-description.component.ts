import { Component, OnInit } from '@angular/core';
import {Position, SchoolPosition} from "../../Models/position";
import {SchoolServiceService} from "../../Services/school/school-service.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-position-description',
  templateUrl: './position-description.component.html',
  styleUrls: ['./position-description.component.css']
})
export class PositionDescriptionComponent implements OnInit {
  position!: Position;
  positionList!: SchoolPosition[];
  positionId! : any;
  constructor(private schoolService: SchoolServiceService, private route : ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
   this.positionId = Number(this.route.snapshot.paramMap.get('id'));
    this.getPosition();
  }

  public getPosition(){
    console.log(this.schoolService.getPositionList())
    this.schoolService.getPositionList().subscribe(
      data => {
        console.log(data);
        this.positionList = data;
        this.Position(this.positionId);
      },
      error => {
        console.log(error);

      });
  }


  private Position(positionId: number){
    for(let schoolPosition of this.positionList){
      if(schoolPosition.position.id == positionId){
        this.position = schoolPosition.position;
      }
    }
  }

  // Speichern die Bewerbung ID bevor das Bewerbungsformular ausgeführt wird
  getPositionId() {
    this.positionId = Number(this.route.snapshot.paramMap.get('id'));
    localStorage.setItem("positionId", this.positionId);
    this.router.navigate(['./application-formular']);
  }
}
