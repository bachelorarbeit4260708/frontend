import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Applicant} from "../../Models/applicant";
import {ApplicantService} from "../../Services/applicant/applicant.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-applicant-registration',
  templateUrl: './applicant-registration.component.html',
  styleUrls: ['./applicant-registration.component.css']
})
export class ApplicantRegistrationComponent implements OnInit {

  registerForm!: FormGroup;

  constructor(private fb: FormBuilder, private userservice: ApplicantService, private router: Router) {
  }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(300)]],
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['', [Validators.required]]
    });
  }

  submitregistration() {
    let formValue: any = this.registerForm.value;
    if (this.registerForm.valid) {
      let result: Applicant = Object.assign({}, formValue);
      this.userservice.postRegistrationData(result).subscribe((data: Applicant) => {
          this.router.navigate(['/applicant-login']);
        },
        error => {
          console.log(!error.errorMessage);
        });
    }
  }
}
