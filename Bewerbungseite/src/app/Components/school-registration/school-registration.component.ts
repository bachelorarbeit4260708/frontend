import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {SchoolServiceService} from "../../Services/school/school-service.service";
import {School} from "../../Models/school";


@Component({
  selector: 'app-school-registration',
  templateUrl: './school-registration.component.html',
  styleUrls: ['./school-registration.component.css']
})
export class SchoolRegistrationComponent implements OnInit {

  companyRegisterForm!: FormGroup;
  wrongCredentials = false;

  constructor(private bf: FormBuilder, private schoolService: SchoolServiceService, private router: Router) {

  }

  ngOnInit(): void {

    this.companyRegisterForm = this.bf.group({
      name: ['',[Validators.required, Validators.maxLength(300)]],
      email: ['',[Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['',[Validators.required]],
      phoneNumber:['',[Validators.required, Validators.maxLength(300)]],
      formationType: ['',[Validators.required, Validators.maxLength(300)]]
    });
  }

  submethode(){
    let formValue: any = this.companyRegisterForm.value;
    if (this.companyRegisterForm.valid) {
      let result: School = Object.assign({}, formValue);
      this.schoolService.postRegistrationData(result).subscribe((data: School) => {
          this.router.navigate(['/school-login']);
        },
        error => {
          console.log(!error.errorMessage);
        });
    }
  }

}
