import {Component, OnInit} from '@angular/core';
import {SchoolServiceService} from "../../Services/school/school-service.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-schools-position',
  templateUrl: './schools-position.component.html',
  styleUrls: ['./schools-position.component.css']
})
export class SchoolsPositionComponent implements OnInit {

  schoolId = localStorage.getItem("loggedId")
  schoolPos!: any;
  pos!: any;

  constructor(private schoolService: SchoolServiceService, private router: Router) {
  }

  ngOnInit(): void {
    this.schoolDisplay();
  }

  schoolDisplay(): void {
    this.schoolService.getSchoolWithPosition(Number(this.schoolId)).subscribe(
      (data) => {
        this.schoolPos = data;
        this.pos = data.position;
      },
      error => {
        console.log(error);

      });

  }

  deletePosition(id: number): void {
    console.log(id)
    this.schoolService.deletePosition(id).subscribe(
      (data) => {
        console.log(data);
      },
      error => {
        console.log(error);
      });
  }
}
