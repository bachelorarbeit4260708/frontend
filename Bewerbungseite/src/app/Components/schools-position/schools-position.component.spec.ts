import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolsPositionComponent } from './schools-position.component';

describe('SchoolsPositionComponent', () => {
  let component: SchoolsPositionComponent;
  let fixture: ComponentFixture<SchoolsPositionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchoolsPositionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SchoolsPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
