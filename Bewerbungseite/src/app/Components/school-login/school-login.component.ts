import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {SchoolServiceService} from "../../Services/school/school-service.service";
import {School} from "../../Models/school";


@Component({
  selector: 'app-school-login',
  templateUrl: './school-login.component.html',
  styleUrls: ['./school-login.component.css']
})
export class SchoolLoginComponent implements OnInit {

  loginForm!: FormGroup;


  constructor(private sl: FormBuilder, private schoolServiceService: SchoolServiceService, private router: Router) {
  }

  ngOnInit(): void {
    this.loginForm = this.sl.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['', [Validators.required]],
    });
  }

  submethode() {
    let values: any = this.loginForm.value;

    if (this.loginForm.valid) {
      this.schoolServiceService.postLoginData(values.email, values.passwort).subscribe((data: School) => {
          console.log(data);
          localStorage.setItem("allSchoolData", JSON.stringify(data));
          this.router.navigate(['/school-profile']).then(() =>
            setTimeout(() => {
              window.location.reload();
            }, 1000)
          );
        },
        error => {
          console.log(!error.errorMessage);
        });
    }
  }


}
