export interface ApplicationForm{
  applicantId : number;
  positionId: number;
  name : string;
  firstName : string;
  dayOfBirth : string;
  gender : string;
  email: string;
  phoneNumber : string;
  streetNumber : string;
  postcode : number;
  city : string;
  country : string;
  file : FormData;
}
