import {ApplicationForm} from "./applicationForm";

export interface Applicant {
  id: number;
  name: string;
  email: string;
  firstName : string;
  password: string;
  dayOfBirth : string;
  gender : string;
  phoneNumber : string;
  streetNumber : string;
  postcode : number;
  city : string;
  country : string;
  uploadDoc : string;
}


