import {Position} from "./position";
import {Address} from "./Address";

export interface School {
  id: number;
  name: string;
  email: string;
  password: string;
  phoneNumber: string;
  formationType: string;
  streetNumber : string,
  postcode : number,
  city : string,
  country : string,
}

export interface SchoolEdit{
  id: number,
  name: string,
  email: string,
  password: string;
  phoneNumber: string;
  formationType: string;
  position: Position,
  address: Address;

}
