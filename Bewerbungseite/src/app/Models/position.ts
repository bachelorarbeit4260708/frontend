import {Address} from "./Address";

export interface Position {
  id: number;
  jobTitel : string,
  jobDescription : string,
  offer : string,
  yourProfile : string;
  contactPersonName : string,
  contactPersonEmail : string,
  //schoolobject: School;
}
export  interface SchoolPosition{
  schoolId : number;
  schoolName: string;
  schoolAddress: Address;
  position: Position;
  formationType:string;
}
