import { Injectable } from '@angular/core';
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  constructor(private router: Router) { }

  logout(){
    let verify: boolean = localStorage.getItem("isSchool") == "true";
    if(verify ==  true ){
      this.router.navigate(['/school-login']);
      localStorage.clear();
      sessionStorage.clear();
    }else {
      this.router.navigate(['/applicant-login']);
      localStorage.clear();
      sessionStorage.clear();
    }

  }
  exist! : boolean;
  isLoggedIn(): boolean{
    let value: Boolean = (localStorage.getItem("isSchool")==null)?false:true;
    if(value == true){
      this.exist = true;
    }else this.exist = false;
    return this.exist;
  }
}
