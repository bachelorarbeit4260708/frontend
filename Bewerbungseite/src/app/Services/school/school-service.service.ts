import { Injectable } from '@angular/core';
import {School, SchoolEdit} from "../../Models/school";
import {map, Observable} from "rxjs";
import {baseApi} from "../../Helpers/Util";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Position, SchoolPosition} from "../../Models/position";


@Injectable({
  providedIn: 'root'
})
export class SchoolServiceService {

  schoolId: any;

  url_root: String = baseApi + "/api/auth";
  position_root: String = baseApi + "/api/position";

  constructor(private http: HttpClient, private router: Router) {
  }

  public postRegistrationData(school: School): Observable<School>{
    return this.http.post<School>(this.url_root + "/signUpSchool",school);
  }

  public postLoginData(email: string, password: string): Observable<School>{
    return this.http.post<School>(this.url_root + "/signinSchool", {
      email, passwort: password
    }).pipe(
      map(result=>{
        this.schoolId  =  result.id;
        console.log("---"+this.schoolId);
        localStorage.setItem("loggedId",this.schoolId);
        localStorage.setItem("isSchool","true");
        return result;
      })
    );
  }

  public getSchoolData(id: number): Observable<School>{
    return this.http.get<School>(this.url_root + "/getSchoolProfilData/"+id );
  }

  public putSchoolProfile(id: number, school: School): Observable<School>{
    return this.http.put<School>(this.url_root + "/putSchoolProfilData/"+ id, school);
  }

  public postAddPosition(position: Position, id: number): Observable<Position>{
    return this.http.post<Position>(this.position_root + "/createPosition/"+ id, position)
  }


  public getPositionList(): Observable<SchoolPosition[]>{
    return this.http.get<SchoolPosition[]>(this.position_root+ "/getAllPosition")
  }

  public getPosition(): Observable<SchoolPosition[]>{
    return this.http.get<SchoolPosition[]>(this.position_root+ "/getPosition")
  }

  public getPositionById(id: number): Observable<Position>{
    return this.http.get<Position>(this.position_root + "/"+id);
  }

  public postUpdatePosition(position: Position): Observable<Position>{
    return this.http.post<Position>(this.position_root + "/update",position)
  }

   public getSchoolWithPosition(id: number): Observable<SchoolEdit>{
    return this.http.get<SchoolEdit>(this.position_root+ "/school/"+id);
   }

   public deletePosition(id: number): Observable<Position>{
    return this.http.get<Position>(this.position_root + "/delete/"+id);
   }
}
