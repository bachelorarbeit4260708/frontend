import {Injectable} from '@angular/core';
import {baseApi} from "../../Helpers/Util";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {map, Observable} from "rxjs";
import {Contact} from "../../Models/contact";
import {Applicant} from "../../Models/applicant";
import {ApplicationForm} from "../../Models/applicationForm";


@Injectable({
  providedIn: 'root'
})
export class ApplicantService {

  userId: any;
  url_root: String = baseApi + "/api/auth";
  url_root2: String = baseApi + "/api/application";

  constructor(private http: HttpClient, private router: Router) {
  }


  public postRegistrationData(applicant: Applicant): Observable<Applicant> {
    return this.http.post<Applicant>(this.url_root + "/signUpApplicant", applicant);
  }


  public postApplicantLogin(email: string, password: string): Observable<Applicant> {
    return this.http.post<Applicant>(this.url_root + "/signInApplicant", {
      email, password
    }).pipe(
      map(result => {
        this.userId = result.id;
        localStorage.setItem("loggedId", this.userId);
        localStorage.setItem("isSchool", "false");
        return result;
      })
    );
  }


  public getApplicantData(id: number): Observable<Applicant> {
    return this.http.get<Applicant>(this.url_root + "/getApplicantProfilData/" + id)
  }


  public putApplicantData(id: number, applicant: Applicant): Observable<Applicant> {
    return this.http.put<Applicant>(this.url_root + "/putApplicantProfilData/" + id, applicant);
  }


  public postContactData(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>(this.url_root + "/contact", contact);
  }


  public createApplication(applicationForm: ApplicationForm, applicantId: number, positionId: number): Observable<ApplicationForm> {

    applicationForm.file.append("applicantId", applicantId.toString());
    return this.http.post<any>(this.url_root2 + "/createApplication/" + positionId, applicationForm.file);
  }

  public createNoAccountApplication(positionId: number, applicationForm: ApplicationForm): Observable<ApplicationForm> {
    applicationForm.file.append("name", applicationForm.name)
    applicationForm.file.append("firstName", applicationForm.firstName)
    applicationForm.file.append("dayOfBirth", applicationForm.dayOfBirth)
    applicationForm.file.append("gender", applicationForm.gender)
    applicationForm.file.append("email", applicationForm.email)
    applicationForm.file.append("phoneNumber", applicationForm.phoneNumber)
    applicationForm.file.append("streetNumber", applicationForm.streetNumber)
    applicationForm.file.append("postCode", applicationForm.postcode + "")
    applicationForm.file.append("city", applicationForm.city)
    applicationForm.file.append("country", applicationForm.country)
    return this.http.post<any>(this.url_root2 + "/createNoAccountApplication/" + positionId, applicationForm.file);
  }
}

