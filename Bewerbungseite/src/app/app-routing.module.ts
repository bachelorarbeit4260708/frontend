import {Component, NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FooterComponent} from "./Components/footer/footer.component";
import {OpenPositionComponent} from "./Components/open-position/open-position.component";
import {ApplicantProfileComponent} from "./Components/applicant-profile/applicant-profile.component";
import {SchoolsPositionComponent} from "./Components/schools-position/schools-position.component";
import {AddTrainingPlaceComponent} from "./Components/add-training-place/add-training-place.component";
import {ApplicationFormularComponent} from "./Components/application-formular/application-formular.component";
import {SchoolLoginComponent} from "./Components/school-login/school-login.component";
import {ConfirmationMessageComponent} from "./Components/confirmation-message/confirmation-message.component";
import {ApplicantFavoriteListComponent} from "./Components/applicant-favorite-list/applicant-favorite-list.component";
import {AboutUsComponent} from "./Components/about-us/about-us.component";
import {ContactComponent} from "./Components/contact/contact.component";
import {FAQComponent} from "./Components/faq/faq.component";
import {HomeComponent} from "./Components/home/home.component";
import {ApplicantLoginComponent} from "./Components/applicant-login/applicant-login.component";
import {SchoolProfileComponent} from "./Components/school-profile/school-profile.component";
import {SchoolRegistrationComponent} from "./Components/school-registration/school-registration.component";
import {ApplicantApplicationsComponent} from "./Components/applicant-applications/applicant-applications.component";
import {ApplicantRegistrationComponent} from "./Components/applicant-registration/applicant-registration.component";
import {PositionDescriptionComponent} from "./Components/position-description/position-description.component";
import {DataProtectionComponent} from "./Components/data-protection/data-protection.component";
import {ImprintComponent} from "./Components/imprint/imprint.component";



const routes: Routes = [
  {path: '', component:HomeComponent},
  {path: 'home', component:HomeComponent},
  {path: 'footer', component:FooterComponent},
  {path: 'open-position', component:OpenPositionComponent},
  {path: 'schools-position', component: SchoolsPositionComponent},
  {path: 'add-training-place', component: AddTrainingPlaceComponent},
  {path: 'update-training-place/:id', component: AddTrainingPlaceComponent},
  {path: 'application-formular', component: ApplicationFormularComponent},
  {path: 'school-login', component:  SchoolLoginComponent},
  {path: 'confirmation-message', component: ConfirmationMessageComponent },
  {path: 'applicant-favorite-list', component: ApplicantFavoriteListComponent},
  {path: 'about-us', component: AboutUsComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'faq', component: FAQComponent},
  {path: 'applicant-login', component: ApplicantLoginComponent},
  {path: 'school-profile', component: SchoolProfileComponent},
  {path: 'school-registration', component: SchoolRegistrationComponent},
  {path: 'applicant-applications', component: ApplicantApplicationsComponent},
  {path: 'position-description/:id', component: PositionDescriptionComponent},
  {path: 'applicant-profile', component: ApplicantProfileComponent},
  {path: 'applicant-registration', component: ApplicantRegistrationComponent},
  {path: 'data-protection', component: DataProtectionComponent},
  {path: 'imprint', component: ImprintComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
