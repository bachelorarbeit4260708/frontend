import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MaterialModul} from "./Material/material.modul";
import { FooterComponent } from './Components/footer/footer.component';
import { OpenPositionComponent } from './Components/open-position/open-position.component';
import {ApplicantProfileComponent} from "./Components/applicant-profile/applicant-profile.component";
import { SchoolsPositionComponent } from './Components/schools-position/schools-position.component';
import { AddTrainingPlaceComponent } from './Components/add-training-place/add-training-place.component';
import { ApplicationFormularComponent } from './Components/application-formular/application-formular.component';
import { SchoolLoginComponent } from './Components/school-login/school-login.component';
import { ConfirmationMessageComponent } from './Components/confirmation-message/confirmation-message.component';
import { ApplicantFavoriteListComponent } from './Components/applicant-favorite-list/applicant-favorite-list.component';
import { AboutUsComponent } from './Components/about-us/about-us.component';
import { ContactComponent } from './Components/contact/contact.component';
import { FAQComponent } from './Components/faq/faq.component';
import { HomeComponent } from './Components/home/home.component';
import { ApplicantLoginComponent } from './Components/applicant-login/applicant-login.component';
import { SchoolProfileComponent } from './Components/school-profile/school-profile.component';
import { SchoolRegistrationComponent } from './Components/school-registration/school-registration.component';
import { ApplicantApplicationsComponent } from './Components/applicant-applications/applicant-applications.component';
import { ApplicantRegistrationComponent } from './Components/applicant-registration/applicant-registration.component';
import { PositionDescriptionComponent } from './Components/position-description/position-description.component';
import { NavbarComponent } from './Components/navbar/navbar.component';
import { DataProtectionComponent } from './Components/data-protection/data-protection.component';
import { ImprintComponent } from './Components/imprint/imprint.component';






@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    OpenPositionComponent,
    ApplicantProfileComponent,
    SchoolsPositionComponent,
    AddTrainingPlaceComponent,
    ApplicationFormularComponent,
    SchoolLoginComponent,
    ConfirmationMessageComponent,
    ApplicantFavoriteListComponent,
    AboutUsComponent,
    ContactComponent,
    FAQComponent,
    HomeComponent,
    ApplicantLoginComponent,
    SchoolProfileComponent,
    SchoolRegistrationComponent,
    ApplicantApplicationsComponent,
    ApplicantRegistrationComponent,
    PositionDescriptionComponent,
    NavbarComponent,
    DataProtectionComponent,
    ImprintComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModul,
    ReactiveFormsModule,
    FormsModule,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http, './assets/i18n/', '.json');
        },
        deps: [HttpClient]
      }
    }),

    FormsModule,
    ReactiveFormsModule
  ],
  exports:[
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
export function HttpLoaderFactory(http:HttpClient){
  return new TranslateHttpLoader(http);
}
