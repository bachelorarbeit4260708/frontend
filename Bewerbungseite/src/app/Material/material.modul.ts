import {NgModule} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatIconModule} from "@angular/material/icon";
import {MatDividerModule} from "@angular/material/divider";
import {MatBadgeModule} from "@angular/material/badge";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatStepperModule} from "@angular/material/stepper";
import {MatSelectModule} from "@angular/material/select";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatListModule} from "@angular/material/list";
import {MatTabsModule} from "@angular/material/tabs";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatMenuModule} from "@angular/material/menu";
import {MatNativeDateModule, MatRippleModule, NativeDateModule} from "@angular/material/core";
import {MatTreeModule} from "@angular/material/tree";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {MatRadioModule} from "@angular/material/radio";

const MaterialCompoment= [

MatButtonModule,
  MatButtonToggleModule,
  MatIconModule,
  MatDividerModule,
  MatBadgeModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatSidenavModule,
  MatFormFieldModule,
  MatCardModule,
  MatStepperModule,
  MatInputModule,
  MatSelectModule,
  MatGridListModule,
  MatExpansionModule,
  MatListModule,
  MatTabsModule,
  MatMenuModule,
  MatRadioModule,
  MatRippleModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatTreeModule,
  NativeDateModule
];

@NgModule(
  {
    imports:[MaterialCompoment],
    exports:[MaterialCompoment]
  }
)
export class MaterialModul{}
