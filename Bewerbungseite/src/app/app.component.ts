import { Component } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Bewerbungseite';


  constructor(private translate: TranslateService, private router: Router) {
    this.translate.addLangs(['de','en']);
    translate.getBrowserCultureLang()?.startsWith('de')?this.translate.setDefaultLang('de'):
      this.translate.setDefaultLang('en');
  }

  // Sprache Überzetzung
  UseLanguage(lang: string){
    this.translate.use(lang);
  }
}
